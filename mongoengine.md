## MongoEngine 中文文档
* 版本0.15.0
* 原作者: Ross Lawley
* 2017/11/29

### 安装
``` pip install -U mongoengine```

### 入门
> 在我们使用mongoengine之前我们先要启动mongod服务器，使用`connect`函数, 如果你是在本机跑的mongod

```
from mongoengine import *
connect('tumblelog')
```

### 定义模型
> MongoDB没有传统ORM的数据库Schema，你可以随意增减数据库的Column，不过定义模型可以帮助我们找bug和定义工具函数等。
> 以一个`Tumblelog`程序举例子, 要存很多不同种类的信息，那我们需要a `collection` of `users`, 然后把`posts`连到`user`上去
> 在`posts`里面，信息多种多样(`text`, `image`, `link`)
> 每一条`post`都可能要带上一个`tag`
> 最好让每一条`post`再带上一个`comment`

* 从`users`说起
> 这样定义`Users`模型, 这个模型与传统ORM的区别是，模型只会留在"application level", 并不会被传到mongodb里面去，这样的好处是你以后可以调整模型架构

```
class User(Document):
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
```

* `Posts`模型
> 所有的`posts`都存在一个`collection`里，每一个类型的post只会存储它需要的数据
> 如果以后要加`视频类型post`，就不用改`collection`了，直接用一个新的`field`
> 我们可以认为`Post`是一个父类，`TextPost``ImagePost``LinkPost`继承`Post`
> 你只需要把`meta`的`allow_inheritance`搞成True即可
> 模型内利用引用存储作者信息，这个引用在实例里面会自动解引用，直接出值

```
class Post(Document):
    title = StringField(max_length=120, required=True)
    author = ReferenceField(User)
    meta = {'allow_inheritance': True}
    
class TextPost(Post):
    content = StringField()
class ImagePost(Post):
    image_path = StringField()
class LinkPost(Post):
    link_url = StringField()
```

* `Tags`模型
> 把tag添加到`Post`上, MongoDB没有外键，但是支持存储list
> `ListField`里可以存储任何类型的值, 不只是`StringField`

```
class Post(Document):
title = StringField(max_length=120, required=True)
author = ReferenceField(User)
tags = ListField(StringField(max_length=30))
```

* `Comments`模型
> 



















